# syntax=docker/dockerfile:1

FROM alpine:3.14

WORKDIR /temp

# install dependencies
RUN apk update && apk upgrade &&\
    apk add ffmpeg &&\
    apk add youtube-dl &&\
    apk add opus-dev &&\
    apk add opusfile-dev

# copy binary file built in the gitlab pipeline
COPY liv /liv

# start command executed once container is started
CMD /liv -token $DISCORD_TOKEN -ytkey $YT_API_KEY
