package main

import (
	"flag"
	"log"
	"os"
	"os/signal"

	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/audio"
	"hainsate.com/liv/src/logger"
	slashcommands "hainsate.com/liv/src/slash-commands"
)

var (
	guildID       = flag.String("guild", "", "Test guild ID. If not passed - bot registers commands globally")
	botToken      = flag.String("token", "", "Bot access token")
	logFile       = flag.String("log", "", "Path to the file used for logging")
	youtubeAPIKey = flag.String("ytkey", "", "youtube api key")
)

func init() {
	flag.Parse()

	if *logFile == "" {
		logger.InitStdOutLogger()

	} else if err := logger.InitLogger(*logFile); err != nil {
		log.Fatalf("Unable to init logger: %v", err)
	}
}

func main() {
	session, err := discordgo.New("Bot " + *botToken)
	if err != nil {
		logger.Fatal("Invalid bot parameters: %v", err)
	}
	session.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		logger.Info("Liv is ready!")
	})

	err = session.Open()
	if err != nil {
		logger.Fatal("Cannot open the session: %v", err)
	}

	audio.YtKey = *youtubeAPIKey
	slashcommands.UpdateSlashCommands(session, *guildID)
	addHandlers(session)

	defer session.Close()

	stopLiv := make(chan os.Signal, 1)
	signal.Notify(stopLiv, os.Interrupt)
	<-stopLiv
	logger.Info("Liv signs out!")
}

func addHandlers(s *discordgo.Session) {
	s.AddHandler(slashcommands.InteractionHandler)
	s.AddHandler(audio.HandleVoiceStateUpdate)
	s.AddHandler(audio.HandleVoiceChannelDeleted)
}
