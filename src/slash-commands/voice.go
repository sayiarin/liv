package slashcommands

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/audio"
)

var PlayCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "play",
		Description: "Liv will join your voice channel and play some music.",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "term",
				Description: "link or search term for a youtube video",
				Required:    true,
			},
		},
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		StandardDefferResponse(s, i)
		term := i.ApplicationCommandData().Options[0].StringValue()

		guild, err := s.State.Guild(i.GuildID)
		if err != nil {
			StandardErrorResponse("Error accessing guild", err, s, i)
			return
		}

		vc := audio.VoiceContextPerGuild[guild.ID]
		if vc == nil {
			vc = audio.NewVoiceContext(guild)
			audio.VoiceContextPerGuild[guild.ID] = vc

			if err := vc.JoinOnUser(i.Member.User.ID, s, i); err != nil {
				StandardErrorResponse("Sadly I cannot join you :<", err, s, i)
				return
			}
		}

		vc.Play(term, s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: false,
	},
}

// with the permission check before every command we can implicitly also make
// sure that a voice context exists so no checks necessary for that in the
// following commands

var PauseCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "pause",
		Description: "Liv will pause playing music",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		vc.PausePlaying()
		StandardResponse("playback paused", s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}

var ResumeCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "resume",
		Description: "Liv will resume playing music",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		vc.ResumePlaying()
		StandardResponse("playback continued", s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}

var SkipCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "skip",
		Description: "skips the currently playing song",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionInteger,
				Name:        "amount",
				Description: "the amount of songs to skip",
				Required:    false,
			},
		},
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		songsToSkip := 1
		if len(i.ApplicationCommandData().Options) == 1 {
			songsToSkip = int(i.ApplicationCommandData().Options[0].IntValue())
			songsToSkip = Clamp(songsToSkip, 1, len(vc.GetQueuedSongs())-1)
		}

		vc.Skip(songsToSkip)
		StandardResponse("skipped song", s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}

var RepeatCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "repeat",
		Description: "repeats the currently played song",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionInteger,
				Name:        "amount",
				Description: "the amount of times the current song should be repeated",
				Required:    false,
			},
		},
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		repeatAmount := 1
		if len(i.ApplicationCommandData().Options) == 1 {
			repeatAmount = int(i.ApplicationCommandData().Options[0].IntValue())
			// it just feels unreasonable queueing one song more than 50 times lol
			repeatAmount = Clamp(repeatAmount, 1, 50)
		}
		vc.Repeat(repeatAmount)

		StandardResponse(fmt.Sprintf("Queued %s %d times", vc.GetCurrentSong().Title, repeatAmount), s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}

var QueueCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "queue",
		Description: "Liv shows you the upcoming songs in the queue",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		if vc == nil {
			StandardResponseToIssuer("Currently I'm not connected to any voice channel", s, i)
			return
		} else if vc.GetCurrentSong() == nil {
			StandardResponseToIssuer("Nothing playing or queued right now", s, i)
			return
		} else {
			StandardDefferResponse(s, i)
		}

		// remove actions from old message
		if vc.GetLatestQueueMessage() != nil {
			s.ChannelMessageEditComplex(&discordgo.MessageEdit{
				Embed:      vc.GetLatestQueueMessage().Embeds[0],
				Components: []discordgo.MessageComponent{},
				ID:         vc.GetLatestQueueMessage().ID,
				Channel:    vc.GetLatestQueueMessage().ChannelID,
			})
		}

		msg, _ := s.ChannelMessageSendComplex(i.ChannelID, GetPagedQueueMessage(vc, 0))
		vc.SetLatestQueueMessage(msg)
		s.InteractionResponseDelete(s.State.User.ID, i.Interaction)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: false,
	},
}

var ShuffleCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "shuffle",
		Description: "Liv will shuffle all songs currently in the queue",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		vc.ShuffleQueue()
		StandardResponse("Songs in queue shuffled around!", s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}

var LeaveCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "leave",
		Description: "Liv will leave the current voice channel",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		vc.Leave()
		StandardResponse("bye :3", s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}

var ClearQueueCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "clear-queue",
		Description: "This will clear the queue",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		vc.ClearQueue()
		StandardResponse("queue cleared", s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}

var NowPlayingCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "now-playing",
		Description: "Displays the currently playing song",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		if vc == nil {
			StandardResponseToIssuer("I'm not connected to any voice channel", s, i)
			return
		}
		NowPlayingResponse(vc, s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: false,
	},
}

var PlayNowCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "playnow",
		Description: "will instantly start playing the selected song from the queue",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionInteger,
				Name:        "index",
				Description: "number of the song in the queue",
				Required:    true,
			},
		},
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		vc := audio.VoiceContextPerGuild[i.GuildID]
		idx := i.ApplicationCommandData().Options[0].IntValue()
		vc.PlayNow(int(idx))
		NowPlayingResponse(vc, s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: true,
	},
}
