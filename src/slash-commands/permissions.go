package slashcommands

import (
	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/audio"
)

type CommandPermissions struct {
	SupportsDMs               bool
	NeedsAdminRights          bool
	NeedsVoiceChannelPresence bool
}

func CheckHasPermissions(perm CommandPermissions, i *discordgo.InteractionCreate) (hasPermissions bool, reason string) {
	hasPermissionsForCommand := true
	reason = ""

	// check for DM support, if command is sent by DM i.Member is not set but i.User is
	if !perm.SupportsDMs && i.Member == nil {
		return false, "command currently not supported in DMs"
	}

	// only relevant for admin permissions
	if perm.NeedsAdminRights {
		hasPermissionsForCommand = userIsAdmin(i)
		reason = "this command nees admin permissions"
	}
	// only allow certain commands (namely the music related ones) if user is in
	// the same voice channel as the bot
	if perm.NeedsVoiceChannelPresence {
		hasPermissionsForCommand = userIsInSameVoiceChannel(i)
		reason = "you need to be in the same voice channel as Liv for this command"
	}
	return hasPermissionsForCommand, reason
}

func userIsAdmin(i *discordgo.InteractionCreate) bool {
	return i.Member.Permissions&(1<<3) == 8
}

func userIsInSameVoiceChannel(i *discordgo.InteractionCreate) bool {
	// no voice context means bot isn't in voice channel on this server
	vc, found := audio.VoiceContextPerGuild[i.GuildID]
	if !found {
		return false
	}

	botVoiceChannel := vc.GetConnction().ChannelID
	for _, vs := range vc.GetGuild().VoiceStates {
		if vs.UserID == i.Member.User.ID {
			// a user can only be in one voice channel in a server so if we find
			// them we can compare bot and user voice channel IDs
			return vs.ChannelID == botVoiceChannel
		}
	}

	return false
}
