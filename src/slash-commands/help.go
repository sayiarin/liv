package slashcommands

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

var HelpCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "help",
		Description: "Shows you a list of all commands",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		StandardDefferResponse(s, i)

		fields := make([]*discordgo.MessageEmbedField, len(SupportedSlashCommands))

		for n, command := range SupportedSlashCommands {
			fields[n] = &discordgo.MessageEmbedField{
				Name:   command.Definition.Name,
				Value:  command.Definition.Description,
				Inline: false,
			}
		}

		creditField := &discordgo.MessageEmbedField{
			// invisible character U+2800 to trick discord :>
			Name:   "⠀",
			Value:  "_find the source code and more info on https://gitlab.com/sayiarin/liv_",
			Inline: false,
		}

		fields = append(fields, creditField)

		s.ChannelMessageSendEmbed(i.ChannelID, &discordgo.MessageEmbed{
			Type: discordgo.EmbedTypeRich,
			Author: &discordgo.MessageEmbedAuthor{
				Name:    s.State.User.Username,
				IconURL: s.State.User.AvatarURL(EmbedAvatarSize),
			},
			Color:  ColorCyan,
			Title:  "All supported commands:",
			Fields: fields,
			Footer: &discordgo.MessageEmbedFooter{
				Text:    fmt.Sprintf("requested by %s", GetRelevantUserName(i)),
				IconURL: i.Member.User.AvatarURL(EmbedAvatarSize),
			},
		})

		s.InteractionResponseDelete(s.State.User.ID, i.Interaction)
	},
}
