package slashcommands

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/logger"
)

var (
	mentionMatcher = regexp.MustCompile("<@![0-9]+>")
	channelMatcher = regexp.MustCompile("<#[0-9]+>")
)

var reactions = [...]string{"1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"}

var PollCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "poll",
		Description: "Create a simple poll for people to vote on with reactions",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "question",
				Description: "The question for the poll",
				Required:    true,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option1",
				Description: "option1",
				Required:    true,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option2",
				Description: "option2",
				Required:    true,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option3",
				Description: "option3",
				Required:    false,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option4",
				Description: "option4",
				Required:    false,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option5",
				Description: "option5",
				Required:    false,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option6",
				Description: "option6",
				Required:    false,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option7",
				Description: "option7",
				Required:    false,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option8",
				Description: "option8",
				Required:    false,
			},
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "option9",
				Description: "option9",
				Required:    false,
			},
		},
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		StandardDefferResponse(s, i)

		question := i.ApplicationCommandData().Options[0].StringValue()
		description := ""

		var mentionedUsers []string
		var mentionedChannels []string

		// replace all user mentions with the user name
		for _, userMention := range mentionMatcher.FindAllString(question, -1) {
			// we know there's <@! in front and > in the back so we cut those off of the userMention string for fetching the user
			if user, err := s.User(userMention[3 : len(userMention)-1]); err == nil {
				question = mentionMatcher.ReplaceAllString(question, user.Username)
				mentionedUsers = append(mentionedUsers, userMention)
			}
		}

		// replace all channel mentions with channel name
		for _, channelMention := range channelMatcher.FindAllString(question, -1) {
			// same as with user mentions
			if channel, err := s.Channel(channelMention[2 : len(channelMention)-1]); err == nil {
				question = channelMatcher.ReplaceAllString(question, channel.Name)
				mentionedChannels = append(mentionedChannels, channelMention)
			}
		}

		if len(mentionedUsers) != 0 {
			description += fmt.Sprintf("_mentioned users: %s_\n", strings.Join(mentionedUsers, ", "))
		}

		if len(mentionedChannels) != 0 {
			description += fmt.Sprintf("_mentioned channels: %s_\n", strings.Join(mentionedChannels, ", "))
		}

		// just add another new line if there's already some text to have a visual distiction
		// between mentions and options
		if description != "" {
			description += "\n"
		}

		for n, o := range i.ApplicationCommandData().Options[1:] {
			description += fmt.Sprintf("%s %s\n", reactions[n], o.StringValue())
		}

		msg, err := s.ChannelMessageSendEmbed(i.ChannelID, &discordgo.MessageEmbed{
			Type: discordgo.EmbedTypeRich,
			Author: &discordgo.MessageEmbedAuthor{
				Name:    GetRelevantUserName(i),
				IconURL: i.Member.User.AvatarURL(EmbedAvatarSize),
			},
			Color:       ColorGreen,
			Title:       question,
			Description: description,
		})
		if err != nil {
			logger.Error("%v", err)
			return
		}

		for n := 0; n < len(i.ApplicationCommandData().Options[1:]); n++ {
			s.MessageReactionAdd(msg.ChannelID, msg.ID, reactions[n])
		}

		s.InteractionResponseDelete(s.State.User.ID, i.Interaction)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: false,
	},
}
