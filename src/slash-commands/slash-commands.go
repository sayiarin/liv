package slashcommands

import (
	"time"

	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/audio"
	"hainsate.com/liv/src/logger"
)

var commands = make(map[string]*SlashCommand)

type SlashCommand struct {
	Definition  discordgo.ApplicationCommand
	Handler     func(s *discordgo.Session, i *discordgo.InteractionCreate)
	Permissions CommandPermissions
}

var SupportedSlashCommands = []*SlashCommand{
	&PingPongCommand,
	&PickCommand,
	&PollCommand,
	&PlayCommand,
	&QueueCommand,
	&ClearQueueCommand,
	&LeaveCommand,
	&SkipCommand,
	&PauseCommand,
	&ResumeCommand,
	&ShuffleCommand,
	&NowPlayingCommand,
	&PlayNowCommand,
	&RepeatCommand,
}

func UpdateSlashCommands(session *discordgo.Session, guildID string) {
	startTime := time.Now()

	appID := session.State.User.ID

	allRegisteredCommands, err := session.ApplicationCommands(appID, guildID)
	if err != nil {
		logger.Fatal("Unable to get already registered commands: %v", err)
	}

	// manually add help command here to avoid circular dependency
	allSupportedCommands := append(SupportedSlashCommands, &HelpCommand)

	for _, slashCommand := range allSupportedCommands {
		indexOfSupportedCommand := -1
		idToUpdate := ""

		// check if currently supported command is already registered
		for i, registeredCommand := range allRegisteredCommands {
			if commandsAreEqual(&slashCommand.Definition, registeredCommand) {
				indexOfSupportedCommand = i
				break
			} else {
				// if they're not equal but have the same name it means we have to update though
				if registeredCommand.Name == slashCommand.Definition.Name {
					idToUpdate = registeredCommand.ID
					break
				}
			}
		}

		// if command needs update we do so now
		if idToUpdate != "" {
			_, err := session.ApplicationCommandEdit(appID, guildID, idToUpdate, &slashCommand.Definition)
			if err != nil {
				logger.Error("Unable to update command %s: %v", slashCommand.Definition.Name, err)
				continue
			}
		} else if indexOfSupportedCommand == -1 {
			// if command is not registered with this configuration then add it
			_, err := session.ApplicationCommandCreate(session.State.User.ID, guildID, &slashCommand.Definition)
			if err != nil {
				logger.Error("Unable to create command %s: %v", slashCommand.Definition.Name, err)
				continue
			}
		} else {
			// otherwise remove proper registered and supported command from the list of currently registered commands
			// we do this by creating a new list appending everything until and after the index of the supported command
			// TODO: maybe find a better solution
			allRegisteredCommands = append(allRegisteredCommands[:indexOfSupportedCommand], allRegisteredCommands[indexOfSupportedCommand+1:]...)
		}

		commands[slashCommand.Definition.Name] = slashCommand
	}

	// unregister all unsupported commands
	for _, command := range allRegisteredCommands {
		err := session.ApplicationCommandDelete(appID, guildID, command.ID)
		if err != nil {
			logger.Error("Unable to delete command %s: %v", command.Name, err)
		}
	}

	elapsedTime := time.Since(startTime)
	logger.Info("Finished updating slash commands in %s", elapsedTime)
}

func InteractionHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	switch i.Type {
	case discordgo.InteractionApplicationCommand:
		if command, found := commands[i.ApplicationCommandData().Name]; found {
			hasPermissions, reason := CheckHasPermissions(command.Permissions, i)
			if !hasPermissions {
				StandardResponseToIssuer(reason, s, i)
				return
			}
			command.Handler(s, i)
		}
	case discordgo.InteractionMessageComponent:
		vc := audio.VoiceContextPerGuild[i.GuildID]
		if vc != nil {
			// right now I only have components for Queue message Previous and
			// Next page, so this is fine. Will need refactoring if more
			// components are added.

			latestQueueMessage := vc.GetLatestQueueMessage()
			// paging
			var newPage int
			if i.MessageComponentData().CustomID == "Next" {
				newPage = vc.GetQueuePage() + 1
			} else {
				newPage = vc.GetQueuePage() - 1
			}
			// update vc queue page
			vc.SetQueuePage(newPage)
			// update message
			msg := GetPagedQueueMessage(vc, newPage)
			// this should be impossible with the current setup, but just to be
			// safe
			if msg != nil {
				s.ChannelMessageEditComplex(&discordgo.MessageEdit{
					Embed:      msg.Embed,
					Components: msg.Components,
					ID:         latestQueueMessage.ID,
					Channel:    latestQueueMessage.ChannelID,
				})
			}
		}

		s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseUpdateMessage,
		})
	}
}

func commandsAreEqual(first *discordgo.ApplicationCommand, second *discordgo.ApplicationCommand) bool {
	if first == nil || second == nil {
		return false
	}
	if first.Name != second.Name {
		return false
	}
	if first.Description != second.Description {
		return false
	}
	if len(first.Options) != len(second.Options) {
		return false
	}
	for i := 0; i < len(first.Options); i++ {
		if !commandOptionsAreEqual(first.Options[i], second.Options[i]) {
			return false
		}
	}

	return true
}

func commandOptionsAreEqual(first *discordgo.ApplicationCommandOption, second *discordgo.ApplicationCommandOption) bool {
	if first == nil || second == nil {
		return false
	}
	if first.Name != second.Name {
		return false
	}
	if first.Description != second.Description {
		return false
	}
	if first.Required != second.Required {
		return false
	}
	if len(first.Choices) != len(second.Choices) {
		return false
	}
	for i := 0; i < len(first.Choices); i++ {
		if first.Choices[i].Name != second.Choices[i].Name {
			return false
		}
		if first.Choices[i].Value != second.Choices[i].Value {
			return false
		}
	}
	if len(first.Options) != len(second.Options) {
		return false
	}
	for i := 0; i < len(first.Options); i++ {
		if !commandOptionsAreEqual(first.Options[i], second.Options[i]) {
			return false
		}
	}

	return true
}
