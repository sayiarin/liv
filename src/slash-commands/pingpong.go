package slashcommands

import "github.com/bwmarrin/discordgo"

var PingPongCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "ping",
		Description: "pings Liv",
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		StandardResponseToIssuer("pong :>", s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: false,
	},
}
