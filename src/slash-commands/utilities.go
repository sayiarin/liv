package slashcommands

import (
	"github.com/bwmarrin/discordgo"
)

func GetRelevantUserName(interaction *discordgo.InteractionCreate) string {
	if interaction.Member.Nick != "" {
		return interaction.Member.Nick
	} else {
		return interaction.Member.User.Username
	}
}

func Clamp(value int, lower int, upper int) int {
	if value < lower {
		return lower
	} else if value > upper {
		return upper
	} else {
		return value
	}
}
