package slashcommands

import (
	"fmt"
	"math"
	"time"

	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/audio"
)

const visibleQueueLength = 12

func GetPagedQueueMessage(vc *audio.VoiceContext, page int) *discordgo.MessageSend {
	queue := vc.GetQueuedSongs()
	maxPages := int(math.Ceil(float64(len(queue)) / float64(visibleQueueLength)))

	// build embed
	embed := audio.GetSongInfoEmbed(vc.GetCurrentSong())
	embed.Description += fmt.Sprintf("\n\n*%d Songs in Queue:*\n", len(queue))

	// get paginated queue
	startIteration := page * visibleQueueLength
	maxIteration := startIteration + visibleQueueLength
	if maxIteration > len(queue) {
		maxIteration = len(queue)
	}

	for i := startIteration; i < maxIteration; i++ {
		embed.Description += fmt.Sprintf("%d - %s\n", i+1, queue[i].Title)
	}

	embed.Description += fmt.Sprintf("\n*Page %d/%d*", page+1, maxPages)

	// calculate total duration, wil lneed a better solution if experience shows
	// this takes too long
	totalDuration := time.Duration(0)
	for _, song := range vc.GetQueuedSongs() {
		totalDuration += song.TotalPlaytime
	}
	embed.Footer = &discordgo.MessageEmbedFooter{
		Text: fmt.Sprintf("Total Playtime: %v", audio.FormatDuration(totalDuration, true)),
	}

	// prepare buttons
	prevButton := discordgo.Button{
		Label:    "Previous Page",
		CustomID: "Prev",
		Style:    discordgo.SecondaryButton,
		Disabled: false,
	}

	nextButton := discordgo.Button{
		Label:    "Next Page",
		CustomID: "Next",
		Style:    discordgo.SecondaryButton,
		Disabled: false,
	}

	// enable/disable buttons depending on page
	if page == 0 {
		prevButton.Disabled = true
	}
	if page == maxPages {
		nextButton.Disabled = true
	}

	return &discordgo.MessageSend{
		Embed: embed,
		Components: []discordgo.MessageComponent{
			discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					prevButton,
					nextButton,
				},
			},
		},
	}
}
