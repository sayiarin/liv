package slashcommands

import (
	"fmt"
	"math/rand"
	"strings"

	"github.com/bwmarrin/discordgo"
)

var PickCommand = SlashCommand{
	Definition: discordgo.ApplicationCommand{
		Name:        "pick",
		Description: "Liv will decide for you by picking one option from a comma separated list",
		Options: []*discordgo.ApplicationCommandOption{
			{
				Type:        discordgo.ApplicationCommandOptionString,
				Name:        "options",
				Description: "The options to choose from separated by a comma (,)",
				Required:    true,
			},
		},
	},
	Handler: func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		options := strings.Split(i.ApplicationCommandData().Options[0].StringValue(), ",")
		for i, option := range options {
			options[i] = strings.TrimSpace(option)
		}

		selection := options[rand.Intn(len(options))]

		formattedUserInput := ""
		for i, option := range options {
			formattedUserInput += fmt.Sprintf("*%s*", option)
			if i == len(options)-1 {
				break
			} else if i == len(options)-2 {
				formattedUserInput += " and "
			} else {
				formattedUserInput += ", "
			}
		}

		response := fmt.Sprintf("If I could choose between %s I think I'd go with **%s** :)", formattedUserInput, selection)

		StandardResponse(response, s, i)
	},
	Permissions: CommandPermissions{
		SupportsDMs:               false,
		NeedsAdminRights:          false,
		NeedsVoiceChannelPresence: false,
	},
}
