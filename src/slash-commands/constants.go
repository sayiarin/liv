package slashcommands

const (
	ColorGreen = 65280
	ColorCyan  = 65535

	EmbedAvatarSize = "64"
)
