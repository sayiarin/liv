package slashcommands

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/audio"
	"hainsate.com/liv/src/logger"
)

func StandardResponse(response string, s *discordgo.Session, i *discordgo.InteractionCreate) {
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: response,
		},
	})
}

func StandardDefferResponse(s *discordgo.Session, i *discordgo.InteractionCreate) {
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
}

func StandardResponseToIssuer(response string, s *discordgo.Session, i *discordgo.InteractionCreate) {
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: response,
			// 1 << 6 is epheral, so only visible to issuer
			Flags: 64,
		},
	})
}

func StandardErrorResponse(response string, err error, s *discordgo.Session, i *discordgo.InteractionCreate) {
	StandardResponseToIssuer(response, s, i)
	logger.Error(fmt.Sprintf("in guild %s channel %s: %v", i.GuildID, i.ChannelID, err))
}

func NowPlayingResponse(vc *audio.VoiceContext, s *discordgo.Session, i *discordgo.InteractionCreate) {
	if song := vc.GetCurrentSong(); song != nil {
		s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "_Currently playing:_",
				Embeds: []*discordgo.MessageEmbed{
					audio.GetSongInfoEmbed(vc.GetCurrentSong()),
				},
			},
		})

	} else {
		StandardResponseToIssuer("Currently I'm not playing any music", s, i)
	}
}
