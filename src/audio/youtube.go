package audio

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"hainsate.com/liv/src/logger"
)

const (
	ytSearchAPI         = "https://youtube.googleapis.com/youtube/v3/search"
	ytVideoAPI          = "https://www.googleapis.com/youtube/v3/videos"
	ytPlaylistAPI       = "https://www.googleapis.com/youtube/v3/playlistItems"
	ytShortLinkHostName = "youtu.be"

	// some youtube API variables
	maxResults              = 50
	maxVideosPerPlaylist    = 200
	maxIDsPerBatchedRequest = 50
)

// a bit hacky, this value will be set in the main function
var YtKey = ""

type YtSearchResult struct {
	Items         []YtSearchResultItem `json:"items"`
	NextPageToken string               `json:"nextPageToken"`
}

type YtSearchResultItem struct {
	// ID will be visible from both APIs but only important for us for
	// videos from the video api endpoint
	ID      string `json:"id"`
	Snippet struct {
		// resourceId/videoId is only visible for playlist api
		ResourceID struct {
			VideoID string `json:"videoId"`
		} `json:"resourceId"`
		Title string `json:"title"`
		// playlist searches have VideoOwnerChannelTitle set, normal videos just
		// the ChannelTitle
		ChannelTitle           string `json:"channelTitle"`
		VideoOwnerChannelTitle string `json:"videoOwnerChannelTitle"`
		Thumbnails             struct {
			Default struct {
				URL string `json:"url"`
			} `json:"default"`
		} `json:"thumbnails"`
		LiveBroadcastContent string `json:"liveBroadcastContent"`
	} `json:"snippet"`
	ContentDetails struct {
		Duration string `json:"duration"`
	} `json:"contentDetails"`
}

func GetSongsByTerm(term string) []*Song {
	songs := make([]*Song, 0)

	if isNotURL(term) {
		searchResult := getSearchResult(term)
		if searchResult == nil || len(searchResult.Items) == 0 {
			return songs
		}
		song := NewSong(&searchResult.Items[0])
		if song == nil {
			return songs
		}
		return []*Song{song}
	}

	songInfo := getSongInfos(term)
	if songInfo == nil {
		return songs
	}

	for _, item := range songInfo.Items {
		song := NewSong(&item)
		if song != nil {
			songs = append(songs, song)
		}
	}

	return songs
}

func getSongInfos(ytUrl string) *YtSearchResult {
	u, _ := url.Parse(ytUrl)
	if u.Hostname() == ytShortLinkHostName {
		id := strings.Replace(u.Path, "/", "", 1)
		if id != "" {
			return getSearchResultFromVideo(id)
		}
	} else {
		params, _ := url.ParseQuery(u.RawQuery)
		if list, found := params["list"]; found {
			if list[0] != "" {
				return getSearchResultFromPlaylist(list[0])
			}
		}
		if video, found := params["v"]; found {
			if video[0] != "" {
				return getSearchResultFromVideo(video[0])
			}
		}
	}
	return nil
}

func getSearchResult(term string) *YtSearchResult {
	searchUrl, _ := url.Parse(ytSearchAPI)
	query := searchUrl.Query()
	query.Set("part", "snippet")
	query.Set("maxResults", "1")
	query.Set("safeSearch", "none")
	query.Set("key", YtKey)
	query.Set("q", term)
	searchUrl.RawQuery = query.Encode()
	return getSearchResultsFromURL(searchUrl)
}

func getSearchResultFromPlaylist(playlistID string) *YtSearchResult {
	result := recursivelyGetPlaylistResult(playlistID, "", 1)
	// fuck youtube, I have to do this because contentDetails is not fully
	// populated with data for playlist items and I need to do a separate
	// request to the video list api, luckily we can supply a list of comma
	// separated IDs
	ids := make([]string, len(result.Items))
	for i, resultItem := range result.Items {
		ids[i] = resultItem.Snippet.ResourceID.VideoID
	}

	// but of course it's not quite that simple, we'll have to cut this list
	// down in smaller pieces
	endResult := YtSearchResult{}
	for i := 0; i < len(ids); i += maxIDsPerBatchedRequest {
		end := i + maxIDsPerBatchedRequest
		if end > len(ids) {
			end = len(ids)
		}
		batchIDs := strings.Join(ids[i:end], ",")
		endResult.Items = append(endResult.Items, getSearchResultFromVideo(batchIDs).Items...)
	}

	return &endResult
}

func recursivelyGetPlaylistResult(playlistID string, nextPageToken string, iterations int) *YtSearchResult {
	searchUrl, _ := url.Parse(ytPlaylistAPI)
	query := searchUrl.Query()
	query.Set("maxResults", strconv.Itoa(maxResults))
	query.Set("key", YtKey)
	query.Set("playlistId", playlistID)
	if nextPageToken != "" {
		query.Set("pageToken", nextPageToken)
	}
	searchUrl.RawQuery = query.Encode()
	// workaround because query.Set() would overwrite value
	searchUrl.RawQuery += "&part=snippet&part=contentDetails"
	searchResult := getSearchResultsFromURL(searchUrl)
	if searchResult != nil && searchResult.NextPageToken != "" && iterations*maxResults < maxVideosPerPlaylist {
		iterations += 1
		nextPageResult := recursivelyGetPlaylistResult(playlistID, searchResult.NextPageToken, iterations)
		searchResult.Items = append(searchResult.Items, nextPageResult.Items...)
	}
	return searchResult
}

func getSearchResultFromVideo(videoID string) *YtSearchResult {
	searchUrl, _ := url.Parse(ytVideoAPI)
	query := searchUrl.Query()
	query.Set("key", YtKey)
	query.Set("id", videoID)
	searchUrl.RawQuery = query.Encode()
	// workaround because query.Set() would overwrite value
	searchUrl.RawQuery += "&part=snippet&part=contentDetails"
	return getSearchResultsFromURL(searchUrl)
}

func getSearchResultsFromURL(url *url.URL) *YtSearchResult {
	response, err := http.Get(url.String())
	if err != nil {
		logger.Info("%v", err)
		return nil
	}

	var searchResult YtSearchResult
	json.NewDecoder(response.Body).Decode(&searchResult)

	if len(searchResult.Items) == 0 {
		return nil
	}

	return &searchResult
}

func isNotURL(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	return err != nil
}
