package audio

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/bwmarrin/discordgo"
	"hainsate.com/liv/src/logger"
)

const timeoutDuration = 5 * time.Minute

var VoiceContextPerGuild = make(map[string]*VoiceContext)

type VoiceContext struct {
	guild               *discordgo.Guild
	connection          *discordgo.VoiceConnection
	songQueue           []*Song
	playState           PlayState
	currentSong         *Song
	timeoutTimer        *time.Timer
	usersInVoiceChannel int

	// for queue pagination we need to keep track of the latest message and
	// current page here
	queueMessage *discordgo.Message
	queuePage    int
}

func (vc *VoiceContext) JoinOnUser(userID string, session *discordgo.Session, interaction *discordgo.InteractionCreate) error {
	voiceChannel := ""
	// find voice channel of user
	for _, vs := range vc.guild.VoiceStates {
		if vs.UserID == userID {
			voiceChannel = vs.ChannelID
			break
		}
	}
	if voiceChannel == "" {
		return fmt.Errorf("sadly I cannot find you in any voice channel. :<")
	}

	// check if busy in another vc
	if vc.connection != nil && vc.connection.ChannelID != voiceChannel {
		return fmt.Errorf("sorry, I am already busy in <#%s>", vc.connection.ChannelID)
	}

	// get initial amount of users inside vc
	for _, vs := range vc.guild.VoiceStates {
		if vs.ChannelID == voiceChannel {
			vc.usersInVoiceChannel++
		}
	}

	// join voice
	var err error
	vc.connection, err = session.ChannelVoiceJoin(vc.guild.ID, voiceChannel, false, true)
	if err != nil {
		return fmt.Errorf("sadly I am not allowed to join your voice channel. :<")
	}

	session.ChannelMessageSend(interaction.ChannelID, fmt.Sprintf("Joined <#%s>", voiceChannel))

	// to allow a reference to the vc in the AfterFunc timer we need to have an
	// anonymous function
	vc.timeoutTimer = time.AfterFunc(timeoutDuration, func() { vc.Leave() })

	return nil
}

func (vc *VoiceContext) PausePlaying() {
	vc.playState = Pause
	vc.timeoutTimer.Reset(timeoutDuration)
}

func (vc *VoiceContext) ResumePlaying() {
	vc.playState = Resume
	vc.timeoutTimer.Stop()
}

func (vc *VoiceContext) Skip(amount int) {
	if amount != 1 {
		vc.songQueue = vc.songQueue[amount-1:]
	}
	vc.playState = Stop
}

func (vc *VoiceContext) Repeat(amount int) {
	repeatedSong := make([]*Song, amount)
	for i := 0; i < amount; i++ {
		repeatedSong[i] = vc.currentSong
	}
	vc.songQueue = append(repeatedSong, vc.songQueue...)
}

func (vc *VoiceContext) PlayNow(idx int) {
	// carry over timer state to after pause, since pause modifies it
	playState := vc.playState
	vc.PausePlaying()
	if playState == Play || playState == Resume {
		vc.timeoutTimer.Stop()
	}

	// first song in queue is 0, but displayed as 1 for user
	idx = idx - 1
	// clamp idx
	if idx < 0 {
		idx = 0
	} else if idx >= len(vc.songQueue) {
		idx = len(vc.songQueue) - 1
	}

	// put song at idx 1 and move all others back
	newSongQueue := make([]*Song, 1, len(vc.songQueue))
	newSongQueue[0] = vc.songQueue[idx]
	newSongQueue = append(newSongQueue, append(vc.songQueue[:idx], vc.songQueue[idx+1:]...)...)
	vc.songQueue = newSongQueue

	// also set currentSong already to the new one so that the bot can give
	// feedback properly; this is fine because the song will terminate when the
	// play state is set to `Stop`with the Skip function
	vc.currentSong = newSongQueue[0]

	// skip to new song
	vc.Skip(1)
}

func (vc *VoiceContext) Leave() {
	vc.songQueue = make([]*Song, 0)
	vc.playState = Stop
	vc.connection.Disconnect()
	delete(VoiceContextPerGuild, vc.guild.ID)
	vc.timeoutTimer.Stop()
}

func (vc *VoiceContext) Play(term string, s *discordgo.Session, i *discordgo.InteractionCreate) {
	vc.addToQueue(term, s, i)
	if vc.currentSong == nil {
		vc.startPlaying()
	}
}

func (vc *VoiceContext) addToQueue(term string, s *discordgo.Session, i *discordgo.InteractionCreate) {
	songs := GetSongsByTerm(term)
	if len(songs) == 0 {
		s.InteractionResponseEdit(s.State.User.ID, i.Interaction, &discordgo.WebhookEdit{
			Content: "_Sadly I cannot add this to the queue, please try a different url or search term_",
		})
		return
	}

	vc.songQueue = append(vc.songQueue, songs...)
	if len(songs) == 1 {
		s.InteractionResponseEdit(s.State.User.ID, i.Interaction, &discordgo.WebhookEdit{
			Content: "_Added one song to the queue:_",
			Embeds: []*discordgo.MessageEmbed{
				GetSongInfoEmbed(songs[0]),
			},
		})
	} else {
		s.InteractionResponseEdit(s.State.User.ID, i.Interaction, &discordgo.WebhookEdit{
			Content: fmt.Sprintf("_Added %s and %d more songs to the queue_", songs[0].Title, len(songs)-1),
			Embeds: []*discordgo.MessageEmbed{
				GetSongInfoEmbed(songs[0]),
			},
		})
	}
}

func (vc *VoiceContext) startPlaying() {
	vc.timeoutTimer.Stop()
	vc.connection.Speaking(true)
	defer vc.connection.Speaking(false)

	for len(vc.songQueue) > 0 {
		vc.playState = Play
		// pop
		vc.currentSong, vc.songQueue = vc.songQueue[0], vc.songQueue[1:]
		if err := vc.currentSong.Play(vc); err != nil {
			logger.Error("error while playing song: %v", err)
		}

	}

	vc.currentSong = nil
	vc.timeoutTimer.Reset(timeoutDuration)
}

func (vc *VoiceContext) ShuffleQueue() {
	for i := len(vc.songQueue) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		vc.songQueue[i], vc.songQueue[j] = vc.songQueue[j], vc.songQueue[i]
	}
}

func (vc *VoiceContext) GetQueuedSongs() []*Song {
	return vc.songQueue
}

func (vc *VoiceContext) ClearQueue() {
	vc.songQueue = make([]*Song, 0)
}

func (vc *VoiceContext) GetCurrentSong() *Song {
	return vc.currentSong
}

func (vc *VoiceContext) GetConnction() *discordgo.VoiceConnection {
	return vc.connection
}

func (vc *VoiceContext) GetGuild() *discordgo.Guild {
	return vc.guild
}

func (vc *VoiceContext) SetLatestQueueMessage(msg *discordgo.Message) {
	vc.queueMessage = msg
	// new message also implies pagination reset to page 0
	vc.queuePage = 0
}

func (vc *VoiceContext) GetLatestQueueMessage() *discordgo.Message {
	return vc.queueMessage
}

func (vc *VoiceContext) SetQueuePage(page int) {
	vc.queuePage = page
}

func (vc *VoiceContext) GetQueuePage() int {
	return vc.queuePage
}

func NewVoiceContext(guild *discordgo.Guild) *VoiceContext {
	vc := new(VoiceContext)
	vc.guild = guild
	vc.connection = nil
	vc.songQueue = make([]*Song, 0)

	vc.playState = Stop
	vc.currentSong = nil

	return vc
}

// handle voice channel updates so that the bot doesn't keep playing music in
// empty channels
func HandleVoiceStateUpdate(s *discordgo.Session, vsUpdate *discordgo.VoiceStateUpdate) {
	// ignore Livs own voice state updates
	if vsUpdate.UserID == s.State.User.ID {
		return
	}

	// update vc user count
	if vc, found := VoiceContextPerGuild[vsUpdate.GuildID]; found {
		before := vc.usersInVoiceChannel

		if vsUpdate.ChannelID == vc.connection.ChannelID {
			vc.usersInVoiceChannel++
		} else if vsUpdate.BeforeUpdate != nil && vsUpdate.BeforeUpdate.ChannelID == vc.connection.ChannelID {
			vc.usersInVoiceChannel--
		}

		// pause if no one else is there, resume if someone joins again
		if vc.usersInVoiceChannel == 0 {
			vc.PausePlaying()
		} else if before == 0 {
			vc.ResumePlaying()
		}
	}
}

// to handle deletion of the voice channel this bot is in
func HandleVoiceChannelDeleted(s *discordgo.Session, vcDelete *discordgo.ChannelDelete) {
	vc := VoiceContextPerGuild[vcDelete.GuildID]
	if vc != nil {
		if vc.connection.ChannelID == vcDelete.ID {
			vc.Leave()
		}
	}
}
