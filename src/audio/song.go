package audio

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"os/exec"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"gopkg.in/hraban/opus.v2"
)

type PlayState uint8

const (
	frameRate     = 48000
	channels      = 2
	frameSize     = 960
	cmdBufferSize = 16384

	// music play stages
	Play   PlayState = 0
	Pause  PlayState = 1
	Paused PlayState = 2
	Resume PlayState = 3
	Stop   PlayState = 4

	ytVideoBaseURL = "https://www.youtube.com/watch?v="

	// output format
	timePlayedCharacterAmount = 16
)

type Song struct {
	ID            string
	Title         string
	ThumbnailURL  string
	WebURL        string
	ChannelName   string
	TotalPlaytime time.Duration

	timePlayed time.Duration
}

func (s *Song) Play(vc *VoiceContext) error {
	ytdl := exec.Command("youtube-dl", "-v", "-f", "bestaudio", "-o", "-", s.ID)
	ytdlOut, err := ytdl.StdoutPipe()
	if err != nil {
		return err
	}

	ytdlBuffer := bufio.NewReaderSize(ytdlOut, cmdBufferSize)

	ffmpeg := exec.Command("ffmpeg", "-i", "pipe:0", "-f", "s16le", "-ar", strconv.Itoa(frameRate), "-ac", strconv.Itoa(channels), "-filter:a", "volume=0.4", "pipe:1")
	ffmpeg.Stdin = ytdlBuffer
	ffmpegOut, err := ffmpeg.StdoutPipe()
	if err != nil {
		return err
	}

	err = ytdl.Start()
	if err != nil {
		return err
	}

	err = ffmpeg.Start()
	if err != nil {
		return err
	}

	ytAudioBuffer := make([]int16, frameSize*channels)
	opusAudioBuffer := make([]byte, frameSize)

	encoder, err := opus.NewEncoder(frameRate, channels, opus.AppAudio)
	if err != nil {
		return err
	}

	encoder.SetBitrateToMax()
	encoder.SetMaxBandwidth(opus.Fullband)

	// keep track of time played
	lastTick := time.Now()

	for {
		switch vc.playState {

		case Stop:
			// completely stop everything
			ffmpeg.Process.Kill()
			ytdl.Process.Kill()
			return nil

		case Pause:
			// suspend running processes
			vc.playState = Paused
			ffmpeg.Process.Signal(syscall.SIGSTOP)
			ytdl.Process.Signal(syscall.SIGSTOP)

		case Paused:
			// do nothing for a short while; this makes it feel "a little bit less responsive"
			// but also doesn't waste cpu time
			time.Sleep(time.Second)

		case Resume:
			// resume stopped processes
			vc.playState = Play
			ffmpeg.Process.Signal(syscall.SIGCONT)
			ytdl.Process.Signal(syscall.SIGCONT)

			//update lastTick for proper time tracking
			lastTick = time.Now()

		case Play:
			// track time
			s.timePlayed += time.Since(lastTick)
			lastTick = time.Now()

			// play music
			err = binary.Read(ffmpegOut, binary.LittleEndian, &ytAudioBuffer)
			if err == io.EOF || err == io.ErrUnexpectedEOF {
				return nil
			}
			if err != nil {
				return err
			}
			_, err = encoder.Encode(ytAudioBuffer, opusAudioBuffer)
			if err != nil {
				return err
			}
			vc.connection.OpusSend <- opusAudioBuffer
		}
	}
}

// may return nil if information not available
func NewSong(r *YtSearchResultItem) *Song {
	if r == nil ||
		r.ID == "" && r.Snippet.ResourceID.VideoID == "" ||
		r.Snippet.Title == "" ||
		r.Snippet.Thumbnails.Default.URL == "" ||
		r.Snippet.ChannelTitle == "" ||
		// no support for live streams yet
		r.Snippet.LiveBroadcastContent != "none" {
		return nil
	}

	song := new(Song)
	// see comments in YtSearchResultItem struct for details
	if r.Snippet.ResourceID.VideoID == "" {
		song.ID = r.ID
	} else {
		song.ID = r.Snippet.ResourceID.VideoID
	}
	song.Title = r.Snippet.Title
	song.ThumbnailURL = r.Snippet.Thumbnails.Default.URL
	song.WebURL = ytVideoBaseURL + song.ID
	if r.Snippet.VideoOwnerChannelTitle != "" {
		song.ChannelName = r.Snippet.VideoOwnerChannelTitle
	} else {
		song.ChannelName = r.Snippet.ChannelTitle
	}
	if r.ContentDetails.Duration != "" {
		// format PT1H1M1S, so we need to remove the leading PT and turn to
		// lower case letters to be able to parse duration
		duration := r.ContentDetails.Duration[2:]
		duration = strings.ToLower(duration)
		song.TotalPlaytime, _ = time.ParseDuration(duration)
	} else {
		song.TotalPlaytime = 0
	}
	song.timePlayed = 0
	return song
}

func GetSongInfoEmbed(song *Song) *discordgo.MessageEmbed {
	{
		playtimePercenatage := song.timePlayed.Seconds() / song.TotalPlaytime.Seconds()

		songLongerThanOneHour := int(song.TotalPlaytime.Hours()) > 0
		playtimeText := FormatDuration(song.timePlayed, songLongerThanOneHour)

		playtimeText += "▕"
		for i := 0; i < timePlayedCharacterAmount; i++ {
			position := int(playtimePercenatage * timePlayedCharacterAmount)
			if i == position {
				playtimeText += "◯"
			} else if i < position {
				playtimeText += "▬"
			} else {
				playtimeText += "━"
			}
		}
		playtimeText += "▏"

		playtimeText += FormatDuration(song.TotalPlaytime, songLongerThanOneHour)

		return &discordgo.MessageEmbed{
			Title:       song.Title,
			Description: fmt.Sprintf("by: %s\n%s", song.ChannelName, playtimeText),
			URL:         song.WebURL,
			Thumbnail: &discordgo.MessageEmbedThumbnail{
				URL: song.ThumbnailURL,
			},
			Color: 65535,
		}
	}
}

func FormatDuration(d time.Duration, needsHour bool) string {
	output := ""
	var minutes time.Duration
	var seconds time.Duration
	if needsHour {
		hours := d / time.Hour
		output += fmt.Sprintf("%02d:", int(hours))

		minutes = (d - hours*time.Hour) / time.Minute
		seconds = (d - hours*time.Hour - minutes*time.Minute) / time.Second
	} else {
		minutes = d / time.Minute
		seconds = (d - minutes*time.Minute) / time.Second
	}

	output += fmt.Sprintf("%02d:%02d", int(minutes), int(seconds))

	return output
}
