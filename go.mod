module hainsate.com/liv

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.3-0.20210821175000-0fad116c6c2a
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20211015200801-69063c4bb744 // indirect
	gopkg.in/hraban/opus.v2 v2.0.0-20210415224706-ab1467d63813
)
