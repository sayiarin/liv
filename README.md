# Liv
Friendly Discord Helper.

[TOC]

# Discontinued
This Project has been Discontinued. With the introduction of discord activities like youtube watch together there are way better methods than streaming audio with a bot. There might be a version 2 in the future in a seperate repo that utilises activities.

# Disclaimer
Youtube and discord both disallow bots to poll and play music or audio from videos by the use of bots in discord. This Project is a personal project meant for study and learning purposes only.
In the end I made this public so that maybe someone can learn from it as there don't seem to be many discord bots out there written in golang and even less with a completely visible setup of a mostly automated build and deployment pipeline.
Using youtube-dl and the youtube api is used as a showcase of how a potential discord golang Bot might utilise third party software and endpoints for additional services.

# Overview
### Technology Stack
A simple bot written in Go using discordgo to access the Discord API and hrabans golang wrapper for the opus audio codec libaries. 

The bot is supposed to be built and deployed via docker so that it can be run on any machine that has docker installed. The docker container defined in the [liv-server.dockerfile](build/liv-server.dockerfile) contains the minimal viable environment for it to run properly.

Gitlab pipelines and public shared runners are utilised to build the docker images and deploy them to a free AWS EC2 instance because for some reason I can't access azures 12 month free VM.

### Features
* play audio from youtube video links, playlist links or find video by search term
* shuffle playlist
* pause, resume, skip functionality
* a handful simple utility functions
* pauses automatically when the last user leaves the channel and resumes once someone joins back
* automatically leaves when playback is paused or stopped for some time 

# How to use
### Self Hosting
If you want to host this bot yourself the easiest way to do so is to create your own discord application and deploy the latest docker image from this repos [docker registry](https://gitlab.com/sayiarin/liv/container_registry/2370430/) and supply your bots App ID. You can also supply your guild ID but that is optional.

### Invite Liv to your discord server
Discord disallows public bots to search and play youtube links. Also doing such would be against youtube TOS, so I won't provide ways to invite the bot.

# Help and Feedback
I am open to feedback and to making improvements on this project anytime. If you have an idea for a new feature or found a bug please feel free to create a [new issue](https://gitlab.com/sayiarin/liv/-/issues/new). I'll gladly consider your merge request as well if you manage to improve Liv yourself!

If you need any help or have some feedback you are also free to join my [discord server](https://discord.hainsate.com) and talk to me there directly. :3

# Licensing and copyright
This project is covered by the [GNU GPLv3 License](LICENSE).
